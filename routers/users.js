import express from 'express'

const router = express.Router();
const users =[
    {
        firstname: "John",
        lastname: "Doe",
        age: 25
    },
    {
        firstname: "Jahe",
        lastname: "Doe",
        age: 24
    }
]

router.get('/',(req,res)=>{
    console.log(users);
    res.send(users);
})

router.post('/',(req,res)=>{
    console.log('Route reached');
    res.send("Completed")
})

export default router;