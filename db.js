
import {createRequire} from 'module'
const require = createRequire(import.meta.url)
const MongoClient = require('mongodb').MongoClient;
// import MongoClient from 'mongodb'
import ObjectID from 'mongodb'
const dbname ="crud_mongodb";
const url ="mongodb://localhost:27017"
const MongoOptions={
    useNewUrlParser : true
}
const state ={
    db : null,

}
export const connect=(cb)=>{
    if(state.db)
    cb();
    else{
        MongoClient.connect(url,MongoOptions,(err,client)=>{
            if(err)
            cb(err)
            else{
                state.db = client.db(dbname)
                cb();
            }
        })
    }
}
export const getPrimaryKey=(_id)=>{
    return ObjectID(_id)
}
 export const getDB=()=>{
    return state.db;
}